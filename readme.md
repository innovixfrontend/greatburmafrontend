## Choc Chip
CHOC-CHIP is the minimalist bundler for modularize frontend projects.

### Start Project

- npm install
- npm install -g gulp [if gulp wasn't installed on global]
- Project Build : gulp

### Local Host :: localhost:8080

### Git Remote
- git init
- git remote add origin (URL)
- git add .
- git commit -m "comment"
- git push
