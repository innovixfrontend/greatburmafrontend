//requiring path and fs modules
const path = require('path');
const fs = require('fs');

let files = fs.readdirSync('./src/assets/js');
let jsFiles = []

// exclude unncessary
for(let i of files) {
  if(i.match('.js') !== null) {
    jsFiles.push('./src/assets/js/' + i);
  }
}

// export function
exports.GET_JSFILE = jsFiles;
