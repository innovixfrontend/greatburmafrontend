$('.banner-slide-section .banner-slide').slick({
  dots: false,
  infinite: true,
  autoplay: false,
  speed: 1000,
  fade: true,
  centerPadding: '40px',
  cssEase: 'linear',
  responsive: [
    {
      breakpoint: 767,
      settings: {
        arrows: false,
        infinite: true,
        centerPadding: '40px',
        slidesToShow: 1,
        autoplay: true,
        fade: true,
      }
    }
  ]
});

$('.slide-card').slick({
  dots: true,
  autoplay: false,
  infinite: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  centerPadding: '20px',
  fade: true,
  cssEase: 'linear',
  responsive: [
    {
      breakpoint: 992,
      settings: {
        autoplay: false,
        infinite: false,
        arrows: false,
        dots: true
      }
    }
  ]
});

//-------------Begin JS for Tradein Upload page------------//
$(window).scroll(function() {
  if ($(this).scrollTop() >= Number('50')) {
    $('.return-to-top').fadeIn(Number('200'));
  } else {
    $('.return-to-top').fadeOut(Number('200'));
  }
});
$(window).scroll(function() {
  if($(this).scrollTop() >= Number('300')) {
    $('.messenger-popup').fadeIn(Number('200'));
  } else {
    $('.messenger-popup').fadeOut(Number('200'));
  }
});
$('.return-to-top').click(function() {
  $('body,html').animate({
    scrollTop : 0
  },Number('500'));
});

$(function() {
  $('.date').datepicker();
})
// date picker
$( function() {
	$( "#datepicker" ).datepicker({
		dateFormat: "dd-mm-yy"
		,duration: "fast"
	});
} );
// -- start booking card --
function mediaSize() { //- cards media size 767px
  if(window.matchMedia('(max-width:767px)').matches) {
    $('.booking-card-all').addClass('show-card');
    $('.booking-card-myanmar').addClass('show-card-1');
    $('.booking-card-asean').addClass('show-card-2');
    $('.travel-info-card').addClass('show-card-3');
  }
}
mediaSize();

// tab 1
$('.show-card').hide();
$('.show-card').slice(0, Number('3')).show();
$('.load-more-all').click(function(){
  $('.show-card:hidden').slice(0, Number('3')).show();
  $('html,body').animate({
      scrollTop: $('.load-more-all').offset().top - Number('1680')
  }, Number('1500'), 'swing');
  if($('.show-card:hidden').length === 0) {
    $('.load-more-all').fadeOut('slow');
  }
});

// tab 2
$('.show-card-1').hide();
$('.show-card-1').slice(0, Number('3')).show();
$('.load-more-myanmar').click(function(){
  $('.show-card-1:hidden').slice(0, Number('3')).show();
  $('html,body').animate({
      scrollTop: $('.load-more-myanmar').offset().top - Number('1680')
  }, Number('1500'), 'swing');
  if($('.show-card-1:hidden').length === 0) {
    $('.load-more-myanmar').fadeOut('slow');
  }
});

// tab 3
$('.show-card-2').hide();
$('.show-card-2').slice(0, Number('3')).show();
$('.load-more-asean').click(function(){
  $('.show-card-2:hidden').slice(0, Number('3')).show();
  $('html,body').animate({
      scrollTop: $('.load-more-asean').offset().top - Number('1680')
  }, Number('1500'), 'swing');
  if($('.show-card-2:hidden').length === 0) {
    $('.load-more-asean').fadeOut('slow');
  }
});
// -- end booking card --

//- start myanmar, asean tour card
$('.tour-card').hide();
$('.tour-card').slice(0, Number('3')).show();

$('.l-myanmar-tour').click(function(){
  $('.tour-card:hidden').slice(0, Number('3')).show();
  $('html,body').animate({
      scrollTop: $('.l-myanmar-tour').offset().top
  }, Number('1500'), 'swing');
  if($('.tour-card:hidden').length === 0) {
    $('.l-myanmar-tour').fadeOut('slow');
  }
});

$('.l-asean-tour').click(function(){
  $('.tour-card:hidden').slice(0, Number('3')).show();
  $('html,body').animate({
      scrollTop: $('.l-asean-tour').offset().top
  }, Number('1500'), 'swing');
  if($('.tour-card:hidden').length === 0) {
    $('.l-asean-tour').fadeOut('slow');
  }
});
//- end myanmar,asean tour card

//- start home page latest tours  section
$('.tour-card-wrapper .tour-card').slice(0, Number('6')).show();
$('.latest-tour').click(function(){
  $('.tour-card:hidden').slice(0, Number('3')).show();
  $('html,body').animate({
      scrollTop: $('.latest-tour').offset().top - Number('500')
  }, Number('1500'), 'swing');
  let hiddenCard = $('.tour-card:hidden');
  if(hiddenCard.length === 0) {
    $('.latest-tour').fadeOut('slow');
  }
});
//- end home page latest tour section

//- start travel info card mobile view
$('.show-card-3').hide();
$('.show-card-3').slice(0, Number('3')).show();
$('.l-travel-info').click(function(){
  $('.show-card-3:hidden').slice(0, Number('3')).show();
  $('html,body').animate({
      scrollTop: $('.l-travel-info').offset().top - Number('1680')
  }, Number('1500'), 'swing');
  if($('.show-card-3:hidden').length === 0) {
    $('.l-travel-info').fadeOut('slow');
  }
});
//- end travel info

$(function() {
  for(let i = 1; i<=10; i++) { // tour search day
    let day = 'Day';
    let days = 'Days'
    if(i<2) {
      $('.select-days').append( 
        '<option value="'+ i +'">' + i + ' ' + day +'</option>'
      );
    } else {
      $('.select-days').append(
        '<option value="'+ i +'">' + i + ' ' + days +'</option>'
      );
    }    
  }

  // contact social
  $('.social').hide();
  $('.contact-us-btn').on('click', function() {
    $('.social').fadeToggle(500);
  });
  // add class card-active for home
  $Cards = $('.why-choose .card');
  $Cards.click(function() {
    $Cards.removeClass('card-active');
    $(this).addClass('card-active');
  });
});
$(document).ready(function () {

//.....social icon 
$('#shareBlock').cShare({
  description: '',
  showButtons: ['fb','ints', 'viber','twitter'],
  data: {
    fb: {
      fa: 'fab fa-facebook-f fb-color',
      name: 'Facebook',
      href: function href(url) {
        return "https://www.facebook.com/sharer.php?u=".concat(url);
      },
      show: true
    },
    ints: {
      fa: 'fab fa-instagram insta-color',
      name: 'Instagram',
      href: function href(url) {
        return "https://www.instagram.com/sharer.php?u=".concat(url);
      },
      show: true
    },
    
    viber: {
      fa: 'fab fa-viber viber-color',
      name: 'Viber',
      href: function href(url) {
        return 'viber://forward?text=' + url;
      },
      show: true
    },
    twitter: {
      fa: 'fab fa-twitter tw-color',
      name: 'Twitter',
      href: function href(url, description) {
        return 'https://twitter.com/intent/tweet?original_referer=' + url + '&url=' + url + '&text=' + description;
      },
      show: false
    },
  }
});
$(".contact-us").on("click", function () {
  $(".social-icns").toggleClass("active");
});
});
